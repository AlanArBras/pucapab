package net.huitel.pucapab.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.huitel.pucapab.R;
import net.huitel.pucapab.database.beans.Product;
import net.huitel.pucapab.ui.custom.ShareButton;

/**
 * Created by jérémy on 13/02/2018.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    // TextView
    public TextView     product_name;
    public TextView     product_barcode;
    public TextView     product_gluten;

    // ImageView
    public ImageView    product_image;

    /**
     * Constructor ViewHolder
     * @param itemView: the itemView
     */
    public ViewHolder(View itemView) {
        super(itemView);
        // link primaryText
        product_name    = (TextView)    itemView.findViewById(R.id.list_item_product_name);
        product_barcode = (TextView)    itemView.findViewById(R.id.list_item_product_barcode);
        product_gluten  = (TextView)    itemView.findViewById(R.id.list_item_product_gluten);
        product_image   = (ImageView)   itemView.findViewById(R.id.list_item_product_image);
    }

    /**
     * Permet d'ajouter un product au RecyclerView
     * @param product   Le produit
     */
    public void bindView(Product product){
        Picasso.with(itemView.getContext()).load("file://"+product.getImagePath()).placeholder(R.drawable.ic_unavailable_image).into(product_image);
        product_name.setText(product.getName());
        product_barcode.setText(product.getBarcode());
        product_gluten.setText(glutenChecker(product.getGluten()));

        // Ajout du bouton de partage
        LinearLayout fl = (LinearLayout) itemView.findViewById(R.id.list_item_product);
        ShareButton shareButton = new ShareButton(itemView.getContext());
        shareButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        shareButton.setText(itemView.getResources().getText(R.string.share_text));
        shareButton.setBarcode(product.getBarcode());
        fl.addView(shareButton);
    }

    /**
     * Savoir à quoi correspond le fait d'avoir du gluten ou pas
     * @param gluten    Le float du gluten stocké en base de données
     * @return          Sa signification
     */
    private String glutenChecker(float gluten){
        /*
            0 : gluten_free
            1 : traces_of_gluten
            2 : contains_gluten
            3 : unknow_presence
            4 : Not found
            5 : error
         */
        if(gluten == 0) {
            itemView.setBackgroundColor(itemView.getResources().getColor(R.color.no_gluten));
            return itemView.getResources().getString(R.string.gluten_free);
        } else if(gluten == 1){
            itemView.setBackgroundColor(itemView.getResources().getColor(R.color.traces_gluten));
            return itemView.getResources().getString(R.string.traces_of_gluten);
        } else if(gluten == 2){
            itemView.setBackgroundColor(itemView.getResources().getColor(R.color.traces_gluten));
            return itemView.getResources().getString(R.string.contains_gluten);
        } else if(gluten == 3){
            itemView.setBackgroundColor(itemView.getResources().getColor(R.color.no_information));
            return itemView.getResources().getString(R.string.presence_of_gluten_unknown);
        } else if(gluten == 4){
            itemView.setBackgroundColor(itemView.getResources().getColor(R.color.no_information));
            return itemView.getResources().getString(R.string.product_not_found);
        } else {
            itemView.setBackgroundColor(itemView.getResources().getColor(R.color.no_information));
            return itemView.getResources().getString(R.string.parsing_error);
        }
    }
}

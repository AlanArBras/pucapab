package net.huitel.pucapab.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.huitel.pucapab.database.beans.Product;

import net.huitel.pucapab.recycler.ViewHolder;

import java.util.List;

/**
 * Created by jérémy on 13/02/2018.
 */

public class RecyclerFragmentAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<Product>   items;
    private int             itemLayout;

    public RecyclerFragmentAdapter(List<Product> items, int itemLayout){
        this.items      = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

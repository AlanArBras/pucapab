package net.huitel.pucapab.mappoint;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MapPoint implements ClusterItem {
    public static final String MAGASIN          = "Magasin";
    public static final String RESTAURANT       = "Restaurant";
    public static final String BOULANGERIE      = "Boulangerie - Patisserie";

    private String title;
    private String snippet;
    private LatLng position;
    private float color;

    public MapPoint(String title, String snippet, double latitude, double longitude, float color){
        this.title      = title;
        this.snippet    = snippet;
        this.position   = new LatLng(latitude, longitude);
        this.color      = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public float getColor() {
        return color;
    }

    public void setColor(float color) {
        this.color = color;
    }
}

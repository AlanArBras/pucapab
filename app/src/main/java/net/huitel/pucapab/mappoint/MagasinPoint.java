package net.huitel.pucapab.mappoint;

/**
 * Created by jérémy on 12/02/2018.
 */

public class MagasinPoint extends MapPoint {
    public MagasinPoint(String title, String snippet, double latitude, double longitude, float color) {
        super(title, snippet, latitude, longitude, color);
    }
}

package net.huitel.pucapab.mappoint;

/**
 * Created by jérémy on 12/02/2018.
 */

public class BoulangeriePoint extends MapPoint {
    public BoulangeriePoint(String title, String snippet, double latitude, double longitude, float color) {
        super(title, snippet, latitude, longitude, color);
    }
}

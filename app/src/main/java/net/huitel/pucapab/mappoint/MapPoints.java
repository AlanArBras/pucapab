package net.huitel.pucapab.mappoint;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jérémy on 12/02/2018.
 */

public class MapPoints {
    private List<MapPoint> points;

    private ClusterManager<MapPoint> cluster;

    public MapPoints(){
        points = new ArrayList<MapPoint>();

        initialisation();
    }

    private void initialisation(){
        // Todo - Regroupage des points par type
        // Magasin
        points.add(new MagasinPoint("Avenir Bio"                                , MapPoint.MAGASIN, 48.129363, -1.639341, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("L'Epicerie au Grand Air"                   , MapPoint.MAGASIN, 48.106957, -1.686413, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Produits de Fleurance"                     , MapPoint.MAGASIN, 48.112262, -1.683605, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Carrés Ronds"                              , MapPoint.MAGASIN, 48.103843, -1.742842, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Scarabée Biocoop Rennes Rue Vasselot"      , MapPoint.MAGASIN, 48.108562, -1.678861, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Scarabée Biocoop St Grégoire"              , MapPoint.MAGASIN, 48.138614, -1.690826, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Scarabée Biocoop Cesson-Sévigné"           , MapPoint.MAGASIN, 48.113635, -1.607269, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Scarabée Biocoop Bruz"                     , MapPoint.MAGASIN, 48.030315, -1.759191, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Pique-Prune Rennes Cleunay"                , MapPoint.MAGASIN, 48.102409, -1.713301, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Azur Bio"                                  , MapPoint.MAGASIN, 48.094075, -1.690432, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("La Vie Claire"                             , MapPoint.MAGASIN, 48.108536, -1.680556, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("La Vie Claire"                             , MapPoint.MAGASIN, 47.224323, -1.550155, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Brin d'Herbe"                              , MapPoint.MAGASIN, 48.080405, -1.601064, BitmapDescriptorFactory.HUE_YELLOW));
        points.add(new MagasinPoint("Biocoop Pays de vitré"                     , MapPoint.MAGASIN, 48.111566, -1.206700, BitmapDescriptorFactory.HUE_YELLOW));

        // Restaurant
        points.add(new RestaurantPoint("Ker Soazig"                                , MapPoint.RESTAURANT, 48.105732, -1.675230, BitmapDescriptorFactory.HUE_GREEN));

        // Boulangerie - Patisserie
        points.add(new BoulangeriePoint("Patisserie Le Daniel Boutique Les Halles"  , MapPoint.BOULANGERIE, 48.108007, -1.678965, BitmapDescriptorFactory.HUE_ORANGE));
        points.add(new BoulangeriePoint("Le Fournil Du Pavail"                      , MapPoint.BOULANGERIE, 48.044213, -1.462604, BitmapDescriptorFactory.HUE_ORANGE));
    }

    public void dessinerPoints(Context context, GoogleMap googleMap){
        cluster = new ClusterManager<MapPoint>(context, googleMap);
        googleMap.setOnCameraIdleListener(cluster);
        googleMap.setOnMarkerClickListener(cluster);

        for(MapPoint mapPoint : points) {
            cluster.addItem(mapPoint);
        }
    }
}

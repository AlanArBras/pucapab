package net.huitel.pucapab.database.beans;

/**
 * Created by root on 2/15/18.
 */

public class IceContact {
    private long id;

    private String displayName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


}

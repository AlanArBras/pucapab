package net.huitel.pucapab.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import net.huitel.pucapab.database.beans.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alan on 09/04/2016.
 * Data Access Object to reach the products table
 */
public class ProductsDAO extends AbstractDAO {

    public static final String PRODUCT_TABLE = "products";
    private static final String ID = "id";
    private static final String BARCODE = "barcode";
    private static final String NAME = "name";
    private static final String GLUTEN = "gluten";
    private static final String IMAGE = "image";

    public static final String CREATE_TABLE = "CREATE TABLE " + PRODUCT_TABLE + "(" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            BARCODE + " NVARCHAR(255), " +
            NAME + " NVARCHAR(255), " +
            GLUTEN + " NVARCHAR(255), " +
            IMAGE + " NVARCHAR(255) " +
            ");";

    /**
     * A String table containing the names of all the columns in the table.
     * It will be used for sending requests to the database.
     */
    private static String[] columns = new String[]{ID, BARCODE, NAME, GLUTEN,IMAGE};

    /**
     * Constructor of a DAO to access products table
     *
     * @param context App context
     * @see AbstractDAO#AbstractDAO(Context)
     */
    public ProductsDAO(Context context) {
        super(context);
    }


    public void insert(Product product) {
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BARCODE, product.getBarcode());
        contentValues.put(NAME, product.getName());
        contentValues.put(GLUTEN, product.getGluten());
        contentValues.put(IMAGE, product.getImagePath());
        long id = db.insert(PRODUCT_TABLE, null, contentValues);
        product.setId(id);
        close();
    }

    public boolean isEmpty() {
        open();
        Cursor cursor = this.db.query(PRODUCT_TABLE, columns, null, null, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        close();
        return count == 0;
    }

    /**
     * @return ArrayList of Product objects containing all the products in the table "products"
     */
    public ArrayList<Product> getProducts() {
        open();
        ArrayList<Product> stock = new ArrayList<>();
        Product product;
        Cursor cursor = this.db.query(PRODUCT_TABLE, columns, null, null, null, null, NAME);
        if (cursor != null) {
            if (cursor.getCount() >= 1) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    product = cursorToProduct(cursor);
                    stock.add(product);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        close();
        return stock;
    }

    /**
     * @return ArrayList of Product objects containing all the products in the table "products" starting with the string "like"
     */
    public ArrayList<Product> getProductsLike(String like) {
        open();
        ArrayList<Product> stock = new ArrayList<>();
        Product product;
        Cursor cursor = this.db.query(PRODUCT_TABLE, columns, NAME + " LIKE ?", new String[] { "%" + like + "%" }, null, null, NAME);
        if (cursor != null) {
            if (cursor.getCount() >= 1) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    product = cursorToProduct(cursor);
                    stock.add(product);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        close();
        return stock;
    }

    public Product getProduct(String productId) {
        open();
        Product product = new Product();
        Cursor cursor = this.db.query(PRODUCT_TABLE, columns, BARCODE + " =?", new String[]{String.valueOf(productId)}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            product = cursorToProduct(cursor);
            cursor.close();
        }
        close();
        return product;
    }


    /**
     * Converts a Cursor object into a Product object.
     *
     * @param cursor Cursor to convert
     * @return Product initialized with the given Cursor's content
     */
    private static Product cursorToProduct(Cursor cursor) {
        Product product = new Product();
        product.setId(cursor.getInt(cursor.getColumnIndex(ID)));
        product.setBarcode(cursor.getString(cursor.getColumnIndex(BARCODE)));
        product.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        product.setGluten(cursor.getInt(cursor.getColumnIndex(GLUTEN)));
        product.setImagePath(cursor.getString(cursor.getColumnIndex(IMAGE)));
        return product;
    }

    public boolean isInLocalDataBase(String barcode) {
        List<Product> productsList = this.getProducts();
        if(productsList != null) {
            for (Product product : productsList) {
                if (product.getBarcode().contains(barcode)) {
                    return true;
                }
            }
        }
        return false;
    }


}

package net.huitel.pucapab.database.beans;

/**
 * Created by root on 1/30/18.
 */

public class Product {
    private long id;
    private String barcode;
    private String name;
    private int gluten;
    private String imagePath;

    public Product(){}

    public long getId() {
        return id;
    }

    public Product setId(long id) {
        this.id = id;
        return this;
    }

    public String getBarcode() {
        return barcode;
    }

    public Product setBarcode(String barcode) {
        this.barcode = barcode;
        return this;
    }

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public int getGluten() {
        return gluten;
    }

    public Product setGluten(int gluten) {
        this.gluten = gluten;
        return this;
    }

    public String getImagePath(){return this.imagePath;}

    public Product setImagePath(String imagePath){
        this.imagePath = imagePath;
        return this;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", barcode='" + barcode + '\'' +
                ", name='" + name + '\'' +
                ", gluten=" + gluten +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }

}
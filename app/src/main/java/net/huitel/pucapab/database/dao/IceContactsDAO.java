package net.huitel.pucapab.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import net.huitel.pucapab.database.beans.IceContact;
import net.huitel.pucapab.database.beans.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 2/15/18.
 */

public class IceContactsDAO extends AbstractDAO {

    public static final String ICE_CONTACTS_TABLE = "ice_contacts";
    private static final String ID = "id";
    private static final String DISPLAY_NAME = "display_name";

    public static final String CREATE_TABLE = "CREATE TABLE " + ICE_CONTACTS_TABLE + "(" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            DISPLAY_NAME + " NVARCHAR(255), " +
            ");";

    /**
     * A String table containing the names of all the columns in the table.
     * It will be used for sending requests to the database.
     */
    private static String[] columns = new String[]{ID, DISPLAY_NAME};

    /**
     * Constructor of a DAO to access products table
     *
     * @param context App context
     * @see AbstractDAO#AbstractDAO(Context)
     */
    public IceContactsDAO(Context context) {
        super(context);
    }


    public void insert(String contactName) {
        if (getNumberOfIceContacts() < 3) {
            open();
            ContentValues contentValues = new ContentValues();
            contentValues.put(DISPLAY_NAME, contactName);
            db.insert(ICE_CONTACTS_TABLE, null, contentValues);
            close();
        }
    }

    public boolean isEmpty() {
        open();
        Cursor cursor = this.db.query(ICE_CONTACTS_TABLE, columns, null, null, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        close();
        return count == 0;
    }

    private int getNumberOfIceContacts() {
        open();
        Cursor cursor = this.db.query(ICE_CONTACTS_TABLE, columns, null, null, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        close();
        return count;
    }

    /**
     * @return ArrayList of Product objects containing all the products in the table "products"
     */
    public ArrayList<IceContact> getIceContacts() {
        open();
        ArrayList<IceContact> iceContacts = new ArrayList<>();
        IceContact iceContact;
        Cursor cursor = this.db.query(ICE_CONTACTS_TABLE, columns, null, null, null, null, DISPLAY_NAME);
        if (cursor != null) {
            if (cursor.getCount() >= 1) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    iceContact = cursorToIceContact(cursor);
                    iceContacts.add(iceContact);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        close();
        return iceContacts;
    }



    /**
     * Converts a Cursor object into a Product object.
     *
     * @param cursor Cursor to convert
     * @return Product initialized with the given Cursor's content
     */
    private static IceContact cursorToIceContact(Cursor cursor) {
        IceContact iceContact = new IceContact();
        iceContact.setId(cursor.getInt(cursor.getColumnIndex(ID)));
        iceContact.setDisplayName(cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)));
        return iceContact;
    }

}

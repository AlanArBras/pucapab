package net.huitel.pucapab.ui.custom;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.huitel.pucapab.firebase.FirebaseNode;
import net.huitel.pucapab.firebase.User;
//import net.huitel.pucapab.utils.SearchableUser;

import java.util.ArrayList;
import java.util.List;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

/**
 * Created by root on 2/13/18.
 */

public class ShareButton extends android.support.v7.widget.AppCompatButton {
    private Context context;
    private ArrayList<User> localUsers;
    private String barcode;

    public ShareButton(final Context context) {
        super(context);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference users = FirebaseDatabase.getInstance().getReference().child(FirebaseNode.USERS);
                users.addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                localUsers = new ArrayList<>();
                                //Get map of users in datasnapshot
                                Iterable<DataSnapshot> users = dataSnapshot.getChildren();
                                for (DataSnapshot user : users) {
                                    User currentUser = user.getValue(User.class);
                                    localUsers.add(currentUser);
                                }

                                showUsers();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                //handle databaseError
                            }
                        });

            }
        });
    }

    /**
     * Affiche la boîte de dialogue permettant d'afficher les utilisateurs
     */
    private void showUsers(){
        new SimpleSearchDialogCompat(getContext(), "Liste d'utilisateurs",
                "Adresse mail...", null, createSampleData(),
                new SearchResultListener<SearchableUser>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           SearchableUser item, int position) {
                        Toast.makeText(getContext(), "Todo : Envoyer le code barre " + barcode + " à " + item.getTitle() ,
                                Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }).show();
    }

    /**
     * Crée la liste des utilisateurs à afficher dans le Dialog
     * @return  La liste des utilisateurs à afficher dans le Dialog
     */
    private ArrayList<SearchableUser> createSampleData() {
        ArrayList<SearchableUser> items = new ArrayList<>();

        for(User user : localUsers){
            items.add(new SearchableUser(user.getEmail()));
        }

        return items;
    }

    public ShareButton setBarcode(String barcode){
        this.barcode = barcode;
        return this;
    }
}

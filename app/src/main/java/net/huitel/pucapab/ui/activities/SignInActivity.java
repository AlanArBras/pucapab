package net.huitel.pucapab.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.huitel.pucapab.R;
import net.huitel.pucapab.firebase.FirebaseNode;
import net.huitel.pucapab.firebase.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import shem.com.materiallogin.DefaultLoginView;
import shem.com.materiallogin.DefaultRegisterView;
import shem.com.materiallogin.MaterialLoginView;
/**
 * Copyright Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class SignInActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {


    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private TextInputLayout mTxtPassword;
    private TextInputLayout mTxtPasswordConfirm;
    private TextInputLayout mTxtEmail;


    @BindView(R.id.sign_in_button)
    SignInButton mSignInButton;

    private GoogleApiClient mGoogleApiClient;

    // Firebase instance variable
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mFirebaseDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        //TODO clean error management

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Initialize FirebaseAuth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        // Initialize MaterialLoginView with two custom views (custom_login_view and custom_register_view)
        // https://android-arsenal.com/details/1/3099#!description
        final MaterialLoginView login = findViewById(R.id.login);
        // Initialize sign in view
        ((DefaultLoginView) login.getLoginView()).setListener(new DefaultLoginView.DefaultLoginViewListener() {
            @Override
            public void onLogin(TextInputLayout loginEmail, TextInputLayout loginPass) {
                //TODO Add spinner during loading
                mTxtEmail = loginEmail;
                mTxtPassword = loginPass;
                if (mTxtEmail.getEditText() != null && mTxtPassword.getEditText() != null) {
                    String email = mTxtEmail.getEditText().getText().toString();
                    if(email.isEmpty()){
                        mTxtEmail.setError(getString(R.string.error_login_badly_formatted_email));
                        mTxtEmail.requestFocus();
                        mTxtPassword.setErrorEnabled(false);
                        return;
                    }

                    String password = mTxtPassword.getEditText().getText().toString();
                    if(password.isEmpty()){
                        mTxtPassword.setError(getString(R.string.error_login_wrong_password));
                        mTxtPassword.requestFocus();
                        mTxtEmail.setErrorEnabled(false);
                        return;
                    }

                    mFirebaseAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(SignInActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "signInWithEmail:success");
                                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                        finish();
                                    } else {
                                        // If sign in fails, display an error
                                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                                        try {
                                            if (task.getException() != null)
                                                throw task.getException();
                                        } catch (FirebaseAuthInvalidUserException e) {
                                            mTxtEmail.setError(getString(R.string.error_login_no_record_corresponding_to_user));
                                            mTxtEmail.requestFocus();
                                            mTxtPassword.setErrorEnabled(false);
                                        } catch (FirebaseAuthInvalidCredentialsException e) {
                                            if (e.getErrorCode().equals("ERROR_WRONG_PASSWORD")) {
                                                mTxtPassword.setError(getString(R.string.error_login_wrong_password));
                                                mTxtPassword.requestFocus();
                                                mTxtEmail.setErrorEnabled(false);
                                            } else if (e.getErrorCode().equals("ERROR_INVALID_EMAIL")) {
                                                mTxtEmail.setError(getString(R.string.error_login_badly_formatted_email));
                                                mTxtEmail.requestFocus();
                                                mTxtPassword.setErrorEnabled(false);
                                            } else {
                                                mTxtEmail.setError(getString(R.string.error_login_wrong_credentials));
                                                mTxtEmail.requestFocus();
                                                mTxtPassword.setErrorEnabled(false);
                                            }
                                        } catch (Exception e) {
                                            Log.e(TAG, e.getMessage());
                                        }
                                    }
                                }
                            });
                }
            }
        });

        // Initialize sign up view
        ((DefaultRegisterView) login.getRegisterView()).setListener(new DefaultRegisterView.DefaultRegisterViewListener() {
            @Override
            public void onRegister(TextInputLayout registerEmail, TextInputLayout registerPass, TextInputLayout registerPassRep) {
                //TODO Add spinner during loading
                mTxtEmail = registerEmail;
                mTxtPassword = registerPass;
                mTxtPasswordConfirm = registerPassRep;

                if (mTxtEmail.getEditText() != null && mTxtPassword.getEditText() != null && mTxtPasswordConfirm.getEditText() != null) {
                    // Check if e-mail string is empty
                    final String email = mTxtEmail.getEditText().getText() != null ? mTxtEmail.getEditText().getText().toString() : "";
                    if (email.isEmpty()) {
                        mTxtEmail.setError(getString(R.string.error_register_empty_email));
                        mTxtEmail.requestFocus();
                        mTxtPassword.setErrorEnabled(false);
                        mTxtPasswordConfirm.setErrorEnabled(false);
                        return;
                    }
                    // Check if password string is empty
                    String pass = mTxtPassword.getEditText().getText() != null ? mTxtPassword.getEditText().getText().toString() : "";
                    if (pass.isEmpty()) {
                        mTxtPassword.setError(getString(R.string.error_register_empty_password));
                        mTxtPassword.requestFocus();
                        mTxtEmail.setErrorEnabled(false);
                        mTxtPasswordConfirm.setErrorEnabled(false);
                        return;
                    }
                    // Check if password confirmation is empty
                    String passRep = mTxtPasswordConfirm.getEditText().getText() != null ? mTxtPasswordConfirm.getEditText().getText().toString() : "";
                    if (!pass.isEmpty() && !pass.equals(passRep)) {
                        mTxtPasswordConfirm.setError(getString(R.string.error_register_password_mismatch));
                        mTxtPasswordConfirm.requestFocus();
                        mTxtEmail.setErrorEnabled(false);
                        mTxtPassword.setErrorEnabled(false);
                        return;
                    }

                    mFirebaseAuth.createUserWithEmailAndPassword(email, pass)
                            .addOnCompleteListener(SignInActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "createUserWithEmail:success");
                                        if(mFirebaseAuth.getCurrentUser() != null) {
                                            String uId = mFirebaseAuth.getCurrentUser().getUid();
                                            User user = new User(email, "");
                                            mFirebaseDatabaseReference.child(FirebaseNode.USERS).child(uId).setValue(user);
                                        }
                                        startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                        finish();
                                    } else {
                                        try {
                                            if (task.getException() != null)
                                                throw task.getException();
                                        } catch (FirebaseAuthWeakPasswordException e) {
                                            mTxtPassword.setError(getString(R.string.error_register_weak_password));
                                            mTxtPassword.requestFocus();
                                            mTxtEmail.setErrorEnabled(false);
                                            mTxtPasswordConfirm.setErrorEnabled(false);
                                        } catch (FirebaseAuthInvalidCredentialsException e) {
                                            mTxtEmail.setError(getString(R.string.error_register_invalid_email));
                                            mTxtEmail.requestFocus();
                                            mTxtPassword.setErrorEnabled(false);
                                            mTxtPasswordConfirm.setErrorEnabled(false);
                                        } catch (FirebaseAuthUserCollisionException e) {
                                            mTxtEmail.setError(getString(R.string.error_register_user_exists));
                                            mTxtEmail.requestFocus();
                                            mTxtPassword.setErrorEnabled(false);
                                            mTxtPasswordConfirm.setErrorEnabled(false);
                                        } catch (Exception e) {
                                            Log.e(TAG, e.getMessage());
                                        }
                                    }
                                }
                            });
                }
            }
        });
    }

    /**
     * "Connect with Google" button's action
     */
    @OnClick(R.id.sign_in_button)
    // Pb rencontré avec clé SHA1: https://github.com/firebase/friendlychat-web/issues/11
    void signInGoogle() {
        //TODO Add spinner during loading
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed
                Log.e(TAG, "Google Sign In failed.");
                Toast.makeText(this, getText(R.string.error_sign_in_failed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * @param acct Account to sign in with
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(SignInActivity.this, getText(R.string.error_sign_in_failed),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            if(mFirebaseAuth.getCurrentUser() != null) {
                                String uId = mFirebaseAuth.getCurrentUser().getUid();
                                String userName = mFirebaseAuth.getCurrentUser().getDisplayName();
                                String email = mFirebaseAuth.getCurrentUser().getEmail();
                                User user = new User(email, userName);
                                mFirebaseDatabaseReference.child(FirebaseNode.USERS).child(uId).setValue(user);
                            }
                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                            finish();
                        }
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }


}

package net.huitel.pucapab.ui.custom;

import ir.mirrajabi.searchdialog.core.Searchable;

/**
 * Created by jérémy on 15/02/2018.
 */

class SearchableUser implements Searchable {
    private String username;

    public SearchableUser(String username) {
        this.username = username;
    }

    @Override
    public String getTitle() {
        return username;
    }

    public SearchableUser setTitle(String title) {
        username = title;
        return this;
    }
}

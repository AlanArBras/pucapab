package net.huitel.pucapab.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.huitel.pucapab.R;
import net.huitel.pucapab.database.beans.Product;
import net.huitel.pucapab.database.dao.ProductsDAO;
import net.huitel.pucapab.utils.ImageStore;

import java.util.Date;

import static com.loopj.android.http.AsyncHttpClient.log;


public class AddProductActivity extends Activity{

    private ImageButton btnTakePhoto;
    private ImageView imgTakenPhoto;
    private EditText editText;

    private Product produit;
    private ProductsDAO produitDao;

    private String barcode;

    private static final int CAM_REQUEST = 1313;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_view);

        btnTakePhoto = (ImageButton) findViewById(R.id.image_btn);
        imgTakenPhoto = (ImageView) findViewById(R.id.new_imageview);
        editText = (EditText) findViewById(R.id.product_name);

        btnTakePhoto.setOnClickListener(new btnTakePhotoClicker());

        produit = new Product();
        produitDao = new ProductsDAO(this);

        Intent intent = getIntent();
        barcode = (intent.getExtras().getString("barcode"));

        if (barcode != null)
            produit.setBarcode(barcode);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CAM_REQUEST)
        {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            imgTakenPhoto.setImageBitmap(thumbnail);

            String imageFileName = new Date().getTime() + getString(R.string.product_photo_format);
            ImageStore.imageSave(this,getString(R.string.product_photo_directory), imageFileName,produit,produitDao, thumbnail);

        }

    }

    class btnTakePhotoClicker implements Button.OnClickListener
    {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            Intent cameraintent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraintent, CAM_REQUEST);
        }

    }

    public void addProductOnClick(View v) {

        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.gluten_state_radioGroup);

        int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
        if (checkedRadioButtonId == R.id.gluten_state_no_rbtn) {
            produit.setGluten(0);
        }
        if (checkedRadioButtonId == R.id.gluten_state_trace_rbtn) {
            produit.setGluten(1);
        }
        if (checkedRadioButtonId == R.id.gluten_state_total_rbtn) {
            produit.setGluten(2);
        }

        if( editText.getText().toString().length() == 0 )
            editText.setError(getString(R.string.required_product));
        else {
            produit.setName(editText.getText().toString());

            produitDao.insert(produit);
            Toast.makeText(this, getString(R.string.product_add), Toast.LENGTH_LONG).show();
            log.i("==========INSERT PRODUCT======>","Produit path: " + produit.getImagePath());
            log.i("==========INSERT PRODUCT======>","Produit barcode: " + produit.getBarcode());
            log.i("==========INSERT PRODUCT======>","Produit name: " + produit.getName());
            log.i("==========INSERT PRODUCT======>","Produit gluten: " + produit.getGluten());
            finish();
        }


    }
}

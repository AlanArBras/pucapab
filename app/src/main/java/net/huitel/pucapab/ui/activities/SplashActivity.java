package net.huitel.pucapab.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import net.huitel.pucapab.PermissionRequest;

/**
 * https://www.bignerdranch.com/blog/splash-screens-the-right-way/
 * Created by alan on 1/9/18.
 */

public class SplashActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                == PackageManager.PERMISSION_GRANTED) {
            startApp();
        } else {
            // Permission is missing and must be requested.
            requestPermission(PermissionRequest.INTERNET);
        }
    }

    /**
     * Starts the SignInActivity or the MainActivity
     */
    void startApp() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        } else {
            // Signed in, launch the MainActivity
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Requests a permission.
     * If an additional rationale should be displayed, the user has to launch the request from
     * a SnackBar that includes additional information.
     */
    private void requestPermission(PermissionRequest permissionRequest) {
        final String manifestPermission = permissionRequest.getManifestPermission();
        final int requestCode = permissionRequest.getRequestCode();

        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                manifestPermission)) {
            Snackbar.make(getWindow().getDecorView().getRootView(), permissionRequest.getExplanation(), Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    ActivityCompat.requestPermissions(SplashActivity.this,
                            new String[]{manifestPermission},
                            requestCode);
                }
            });
        } else {
            // Request the permission. The result will be received in onRequestPermissionResult().
            ActivityCompat.requestPermissions(this, new String[]{manifestPermission},
                    requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionRequest.INTERNET.getRequestCode()) {
            // Request for CALL_PHONE permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startApp();
            } else {
                // Permission request was denied.
                Snackbar.make(getWindow().getDecorView().getRootView(), "Autorisation d'accès à Internet refusée, impossible de se connecter",
                        Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }
}

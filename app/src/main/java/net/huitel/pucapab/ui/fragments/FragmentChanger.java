package net.huitel.pucapab.ui.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import net.huitel.pucapab.R;

/**
 * Created by jérémy on 13/02/2018.
 */

public class FragmentChanger {
    public static final String TAG_HOME = "HOME";
    public static final String TAG_LIST = "LIST";

    /**
     * Change le fragment courant
     * @param activity  Activité
     * @param fragment  Classe du fragment
     */
    public static void change(AppCompatActivity activity, Fragment fragment, String tag){
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_place, fragment, tag);
        fragmentTransaction.addToBackStack(tag);

        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();
    }

    /**
     * Fait un retour en arrière
     * @param activity  Activité
     */
    public static void back(AppCompatActivity activity){
        activity.getSupportFragmentManager().popBackStack();
    }

    /**
     * Checke voir si le fragment courant est le fragment principal
     * @param activity  Activité
     * @return  TRUE    Si le fragment est le fragment principal
     *          FALSE   Sinon
     */
    public static boolean isOnHome(AppCompatActivity activity){
        return activity.getSupportFragmentManager().getBackStackEntryCount() == 1;
    }
}

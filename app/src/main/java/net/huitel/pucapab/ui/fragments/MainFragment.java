package net.huitel.pucapab.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.budiyev.android.codescanner.AutoFocusMode;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ErrorCallback;
import com.google.zxing.Result;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import net.huitel.pucapab.PermissionRequest;
import net.huitel.pucapab.R;
import net.huitel.pucapab.database.beans.Product;
import net.huitel.pucapab.database.dao.ProductsDAO;
import net.huitel.pucapab.network.GlutenChecker;
import net.huitel.pucapab.network.HttpUtils;
import net.huitel.pucapab.ui.activities.AddProductActivity;
import net.huitel.pucapab.ui.custom.ShareButton;
import net.huitel.pucapab.utils.ImageStore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

import static com.google.android.gms.internal.zzahn.runOnUiThread;

/**
 * Created by jérémy on 13/02/2018.
 */

public class MainFragment extends Fragment implements ActivityCompat.OnRequestPermissionsResultCallback {
    private CodeScanner mCodeScanner;
    private CodeScannerView scannerView;
    private ImageView mainButton;
    private ImageView productImage;
    private TextView instruction;
    Product product;
    ProductsDAO productsDAO;

    private ShareButton shareButton;

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment_content, container, false);

        instruction = view.findViewById(R.id.instruction);
        scannerView = view.findViewById(R.id.scanner_view);
        scannerView.setAutoFocusButtonVisible(false);
        scannerView.setVisibility(View.GONE);
        //check wether the app is still connected to internet
        // Use builder
        mCodeScanner = CodeScanner.builder()
                /*camera can be specified by calling .camera(cameraId),
                first back-facing camera on the device by default*/
                /*code formats*/
                .formats(CodeScanner.ALL_FORMATS)/*List<BarcodeFormat>*/
                /*or .formats(BarcodeFormat.QR_CODE, BarcodeFormat.DATA_MATRIX, ...)*/
                /*or .format(BarcodeFormat.QR_CODE) - only one format*/
                /*auto focus*/
                .autoFocus(true).autoFocusMode(AutoFocusMode.SAFE).autoFocusInterval(2000L)
                /*flash*/
                .flash(false)
                /*decode callback*/
                .onDecoded(new DecodeCallback() {
                    @Override
                    public void onDecoded(@NonNull final Result result) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String barcode = result.getText();
                                if (barcode != null && !barcode.isEmpty()) {
                                    checkGluten(barcode);
                                }
                                mCodeScanner.stopPreview();
                                scannerView.setVisibility(View.GONE);
                                instruction.setVisibility(View.GONE);
                                mainButton.setVisibility(View.VISIBLE);

                                shareButton.setBarcode(barcode).setVisibility(View.VISIBLE);
                            }
                        });
                    }
                })
                /*error callback*/
                .onError(new ErrorCallback() {
                    @Override
                    public void onError(@NonNull final Exception error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("MainFragment", error.getMessage());
                            }
                        });
                    }
                }).build(view.getContext(), scannerView);
        // Or use constructor to create scanner with default parameters
        // All parameters can be changed after scanner created
        // mCodeScanner = new CodeScanner(this, scannerView);
        // mCodeScanner.setDecodeCallback(...);
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
        mainButton = view.findViewById(R.id.floatingActionButton);
        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO Gérer une animation de fading pour l'apparition de la caméra
                scanNow(view);
            }
        });
        mainButton.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));


        //set the properties for button
        FrameLayout fl = (FrameLayout) view.findViewById(R.id.sign_in_layout);
        shareButton = new ShareButton(this.getContext());
        shareButton.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        shareButton.setText(getText(R.string.share_text));
        shareButton.setVisibility(View.GONE);

        //add button to the layout
        fl.addView(shareButton);

        return view;
    }

    @Override
    public void onPause() {
        mCodeScanner.setFlashEnabled(false);
        mCodeScanner.stopPreview();
        mCodeScanner.releaseResources();
        scannerView.setVisibility(View.GONE);
        super.onPause();
    }

    /**
     * event handler for scan button
     *
     * @param view view of the activity
     */
    public void scanNow(View view) {
        // BEGIN_INCLUDE(scanNow)
        // Check if the Camera permission has been granted
        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview
            mCodeScanner.setFlashEnabled(false);
            scannerView.setVisibility(View.VISIBLE);
            mainButton.setVisibility(View.GONE);
            instruction.setVisibility(View.GONE);
            shareButton.setVisibility(View.GONE);
            mCodeScanner.startPreview();
        } else {
            // Permission is missing and must be requested.
            requestPermission(PermissionRequest.CAMERA);
        }
        // END_INCLUDE(scanNow)

    }


    /**
     * Method to check Gluten presence and create product into local database or read into the local database if the product was already loaded from the JSON API
     *
     * @param barcode
     */
    private void checkGluten(final String barcode) {
        //TODO Ajouter une animation au niveau du bouton principal pour indiquer le chargement pendant la recherche
        //TODO Ajouter une détection de l'accès à internet: demande d'activation du Wifi ou de la 4G si besoin
        productsDAO = new ProductsDAO(view.getContext());
        if (!productsDAO.isInLocalDataBase(barcode) && isConnected()) {
            product = new Product();
            product.setBarcode(barcode);
            HttpUtils.get(barcode, new RequestParams(), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    String text;
                    int color;
                    switch (GlutenChecker.getGlutenPresence(response)) {
                        case GLUTEN:
                            text = getString(R.string.contains_gluten);
                            color = ContextCompat.getColor(view.getContext(), R.color.presence_gluten);
                            product.setGluten(2);
                            break;
                        case TRACES:
                            text = getString(R.string.traces_of_gluten);
                            color = ContextCompat.getColor(view.getContext(), R.color.traces_gluten);
                            product.setGluten(1);
                            break;
                        case GLUTEN_FREE:
                            text = getString(R.string.gluten_free);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_gluten);
                            product.setGluten(0);
                            break;
                        case UNKNOWN:
                            text = getString(R.string.presence_of_gluten_unknown);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                            product.setGluten(3);
                            break;
                        case NOT_FOUND:

                            Snackbar snackbar = Snackbar
                                    .make(scannerView,getString(R.string.ajouter_le_nouveau_produit),Snackbar.LENGTH_INDEFINITE)
                                    .setAction("OUI", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent myIntent = new Intent(getActivity(), AddProductActivity.class);
                                            myIntent.putExtra("barcode", barcode + "");
                                            startActivity(myIntent);
                                        }
                                    });
                            snackbar.setActionTextColor(Color.WHITE);
                            snackbar.show();
                            text = getString(R.string.product_not_found);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                            product.setGluten(4);
                            break;
                        default:
                            text = getString(R.string.presence_of_gluten_unknown);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                            product.setGluten(3);
                            break;
                    }
                    //TODO : filtrer si il est dispo dans la base de données
                    String productName = GlutenChecker.getProductName(response);
                    ((TextView) view.findViewById(R.id.product_name)).setText(productName);
                    product.setName(productName);
                    ((TextView) getView().findViewById(R.id.gluten_state)).setText(text);
                    ((TextView) getView().findViewById(R.id.gluten_state)).setTextColor(color);
                    productImage = (ImageView) getView().findViewById(R.id.product_image);

                    //Set visible to Image View
                    Picasso.with(view.getContext()).load(GlutenChecker.getProductImage(response)).placeholder(R.drawable.ic_unavailable_image).into(productImage);
                    String imageFileName = new Date().getTime() + getString(R.string.product_photo_format);

                    Picasso.with(view.getContext()).load(GlutenChecker.getProductImage(response)).into(ImageStore.picassoImageTarget(view.getContext(),getString(R.string.product_photo_directory), imageFileName,product,productsDAO));
                    //Product Insertion into local base
                    //Todo : Problème de thread, attendre que image Store soit terminé
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                    // Pull out the first event on the public timeline*
                    String text;
                    int color;
                    switch (GlutenChecker.getGlutenPresence(timeline)) {
                        case GLUTEN:
                            text = getString(R.string.contains_gluten);
                            color = ContextCompat.getColor(view.getContext(), R.color.presence_gluten);
                            product.setGluten(2);
                            break;
                        case TRACES:
                            text = getString(R.string.traces_of_gluten);
                            color = ContextCompat.getColor(view.getContext(), R.color.traces_gluten);
                            product.setGluten(1);
                            break;
                        case GLUTEN_FREE:
                            text = getString(R.string.gluten_free);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_gluten);
                            product.setGluten(0);
                            break;
                        case UNKNOWN:
                            text = getString(R.string.presence_of_gluten_unknown);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                            product.setGluten(3);
                            break;
                        case ERROR:
                            text = getString(R.string.parsing_error);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                            product.setGluten(5);
                            break;
                        case NOT_FOUND:
                            text = getString(R.string.product_not_found);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                            product.setGluten(4);
                            break;
                        default:
                            text = getString(R.string.presence_of_gluten_unknown);
                            color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                            product.setGluten(3);
                            break;
                    }
                }
            });

        }
        //Get data in localdatabase
        else {
            String text;
            int color;
            if (productsDAO.getProduct(barcode) != null) {
                product = productsDAO.getProduct(barcode);
                switch (product.getGluten()) {
                    //Gluten free
                    case 0:
                        text = getString(R.string.gluten_free);
                        color = ContextCompat.getColor(view.getContext(), R.color.no_gluten);
                        break;
                    case 1:
                        text = getString(R.string.traces_of_gluten);
                        color = ContextCompat.getColor(view.getContext(), R.color.traces_gluten);
                        break;
                    case 2:
                        text = getString(R.string.contains_gluten);
                        color = ContextCompat.getColor(view.getContext(), R.color.presence_gluten);
                        break;
                    case 3:
                        text = getString(R.string.presence_of_gluten_unknown);
                        color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                        break;
                    case 4:
                        text = getString(R.string.product_not_found);
                        color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                        break;
                    case 5:
                        text = getString(R.string.parsing_error);
                        color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                        break;
                    default:
                        text = getString(R.string.presence_of_gluten_unknown);
                        color = ContextCompat.getColor(view.getContext(), R.color.no_information);
                        break;
                }
                ((TextView) getView().findViewById(R.id.gluten_state)).setText(text);
                ((TextView) getView().findViewById(R.id.gluten_state)).setTextColor(color);
                productImage = (ImageView) getView().findViewById(R.id.product_image);
                ((TextView) getView().findViewById(R.id.product_name)).setText(product.getName());
                Picasso.with(view.getContext()).load("file://"+product.getImagePath()).placeholder(R.drawable.ic_unavailable_image).into(productImage);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        // BEGIN_INCLUDE(onRequestPermissionsResult)
        if (requestCode == PermissionRequest.CAMERA.getRequestCode()) {
            // Request for camera permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                scanNow(scannerView);
            } else {
                // Permission request was denied.
                Snackbar.make(scannerView, "Autorisation d'accès à la caméra refusée, impossible de lancer le scanner",
                        Snackbar.LENGTH_LONG)
                        .show();
            }
        }
        // END_INCLUDE(onRequestPermissionsResult)
    }

    /**
     * Requests a permission.
     * If an additional rationale should be displayed, the user has to launch the request from
     * a SnackBar that includes additional information.
     */
    private void requestPermission(PermissionRequest permissionRequest) {
        final String manifestPermission = permissionRequest.getManifestPermission();
        final int requestCode = permissionRequest.getRequestCode();

        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                manifestPermission)) {
            Snackbar.make(scannerView, permissionRequest.getExplanation(), Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{manifestPermission},
                            requestCode);
                }
            });
        } else {
            // Request the permission. The result will be received in onRequestPermissionResult().
            ActivityCompat.requestPermissions(getActivity(), new String[]{manifestPermission},
                    requestCode);
        }
    }

    /**
     * Check if the device is connected to internet MOBILE network or WIFI
     */
    public boolean isConnected() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}

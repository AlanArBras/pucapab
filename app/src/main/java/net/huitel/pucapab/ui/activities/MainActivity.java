package net.huitel.pucapab.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.huitel.pucapab.PermissionRequest;
import net.huitel.pucapab.R;
import net.huitel.pucapab.ui.fragments.FragmentChanger;
import net.huitel.pucapab.ui.fragments.MainFragment;
import net.huitel.pucapab.ui.fragments.RecyclerFragment;

import java.util.TreeSet;

import mx.com.quiin.contactpicker.SimpleContact;
import mx.com.quiin.contactpicker.ui.ContactPickerActivity;

/**
 * https://android-arsenal.com/details/1/6510
 */
public class MainActivity extends AppCompatActivity
        implements ActivityCompat.OnRequestPermissionsResultCallback, NavigationView.OnNavigationItemSelectedListener {

    FirebaseAuth mFirebaseAuth;
    FirebaseUser mFirebaseUser;
    private static final int CONTACT_PICKER_REQUEST = 9001;

    private ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Le fragment principal qui se charge QUE si on vient de lancer l'app
        if(null == savedInstanceState) {
            FragmentChanger.change(this, new MainFragment(), FragmentChanger.TAG_HOME);
        }

        // Les autres trucs

        constraintLayout = (ConstraintLayout) findViewById(R.id.fragment_place);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        MenuItem connectionItem = navigationView.getMenu().findItem(R.id.connection_item);

        //TODO: connexion anonyme si encore du temps
        if (mFirebaseUser != null) {
            // User is signed in
            connectionItem.setTitle(getText(R.string.sign_out));
        } else {
            // No user is signed in
            connectionItem.setTitle(getText(R.string.sign_in));
        }

        MenuItem iceContact = navigationView.getMenu().findItem(R.id.ice_contact);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            FragmentChanger.change(this, new MainFragment(), FragmentChanger.TAG_HOME);
        } /*else if (scannerView.getVisibility() == View.VISIBLE) {
            mCodeScanner.stopPreview();
            scannerView.setVisibility(View.GONE);
            mainButton.setVisibility(View.VISIBLE);
        }*/
        else if(! FragmentChanger.isOnHome(this)) {
            FragmentChanger.back(this);
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_local_list) {
            FragmentChanger.change(this, new RecyclerFragment(), FragmentChanger.TAG_LIST);
        } else if (id == R.id.nav_manage) {
            Snackbar.make(constraintLayout, "Outils à venir", Snackbar.LENGTH_LONG).show();
        } else if (id == R.id.nav_map) {
            String username = mFirebaseUser.getEmail();
            Intent intentMap = new Intent(this, MapActivity.class);
            intentMap.putExtra("username", username);
            startActivity(intentMap);
        } else if (id == R.id.nav_send) {
            Snackbar.make(constraintLayout, "Fonctionnalité d'envoi de logs à venir", Snackbar.LENGTH_LONG).show();
        } else if (id == R.id.call_bro) {
            callEmergency(constraintLayout);
        } else if (id == R.id.connection_item) {
            mFirebaseAuth.signOut();
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
            finish();
        } else if (id == R.id.ice_contact){
            if (ActivityCompat.checkSelfPermission(this, PermissionRequest.READ_CONTACTS.getManifestPermission())
                    == PackageManager.PERMISSION_GRANTED) {
                Intent contactPicker = new Intent(this, ContactPickerActivity.class);
                startActivityForResult(contactPicker, CONTACT_PICKER_REQUEST);

            } else{
                requestPermission(PermissionRequest.READ_CONTACTS);
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    private void callEmergency(View view) {
        Snackbar.make(view, getResources().getString(R.string.call_snackbar_text), Snackbar.LENGTH_SHORT)
                .setActionTextColor(getResources().getColor(R.color.white))
                .setAction(getResources().getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        call(true);
                    }
                })
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                        if (event != DISMISS_EVENT_ACTION && event != DISMISS_EVENT_CONSECUTIVE)
                            call(false);
                    }
                }).show();
    }

    /**
     * If canceled is not true, then a CALL Intent is launched,
     * nothing is done otherwise.
     *
     * @param canceled boolean, the call is canceled if true.
     */
    private void call(boolean canceled) {
        if (!canceled) {
            // Check if the CALL_PHONE permission has been granted
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                // Permission is already available, start call
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + getResources().getInteger(R.integer.ermergency_number)));
                startActivity(callIntent);
            } else {
                // Permission is missing and must be requested.
                requestPermission(PermissionRequest.CALL_PHONE);
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case CONTACT_PICKER_REQUEST:
                if(resultCode == RESULT_OK){
                    TreeSet<SimpleContact> selectedContacts = (TreeSet<SimpleContact>)data.getSerializableExtra(ContactPickerActivity.CP_SELECTED_CONTACTS);
                    for (SimpleContact selectedContact : selectedContacts)
                        Log.e("Selected", selectedContact.toString());
                }else
                    Toast.makeText(this, "No contacts selected", Toast.LENGTH_LONG).show();
                break;
            default:
                super.onActivityResult(requestCode,resultCode,data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        // BEGIN_INCLUDE(onRequestPermissionsResult)
        if (requestCode == PermissionRequest.CALL_PHONE.getRequestCode()) {
            // Request for CALL_PHONE permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callEmergency(constraintLayout);
            } else {
                // Permission request was denied.
                Snackbar.make(constraintLayout, "Autorisation d'accès à la fonction d'appel refusée, impossible d'appeler le BRO",
                        Snackbar.LENGTH_LONG)
                        .show();
            }
        } else if (requestCode == PermissionRequest.INTERNET.getRequestCode()) {
            // Request for CALL_PHONE permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callEmergency(constraintLayout);
            } else {
                // Permission request was denied.
                Snackbar.make(constraintLayout, "Autorisation d'accès à internet refusée, impossible de se connecter",
                        Snackbar.LENGTH_LONG)
                        .show();
            }
        }
        // END_INCLUDE(onRequestPermissionsResult)
    }

    /**
     * Requests a permission.
     * If an additional rationale should be displayed, the user has to launch the request from
     * a SnackBar that includes additional information.
     */
    private void requestPermission(PermissionRequest permissionRequest) {
        final String manifestPermission = permissionRequest.getManifestPermission();
        final int requestCode = permissionRequest.getRequestCode();

        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                manifestPermission)) {
            Snackbar.make(constraintLayout, permissionRequest.getExplanation(), Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{manifestPermission},
                            requestCode);
                }
            });
        } else {
            // Request the permission. The result will be received in onRequestPermissionResult().
            ActivityCompat.requestPermissions(this, new String[]{manifestPermission},
                    requestCode);
        }
    }
}

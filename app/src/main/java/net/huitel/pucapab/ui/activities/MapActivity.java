package net.huitel.pucapab.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.huitel.pucapab.PermissionRequest;
import net.huitel.pucapab.R;
import net.huitel.pucapab.mappoint.MapPoints;

/**
 * Created by jérémy on 11/02/2018.
 */

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private GoogleMap googleMap;

    private MapPoints mapPoints;

    private Marker currentMarker;

    private String currentUsername;

    private Button backButton;

    private View map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        map = (View) findViewById(R.id.map);

        // Chargement des points de repère de la map
        mapPoints = new MapPoints();

        currentMarker = null;

        currentUsername = getIntent().getExtras().getString("username");

        backButton = (Button) findViewById(R.id.map_back);
        backButton.setOnClickListener(this);

        // Affichage de la carte Google Maps
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        initialize();
    }

    /**
     * Permet d'initialiser la map
     */
    public void initialize(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Activation du ACCESS_FINE_LOCATION
            ActivityCompat.requestPermissions(MapActivity.this,
                    new String[]{PermissionRequest.ACCESS_FINE_LOCATION.getManifestPermission()},
                    PermissionRequest.ACCESS_FINE_LOCATION.getRequestCode());

            // Activation du ACCESS_COARSE_LOCATION
            ActivityCompat.requestPermissions(MapActivity.this,
                    new String[]{PermissionRequest.ACCESS_COARSE_LOCATION.getManifestPermission()},
                    PermissionRequest.ACCESS_COARSE_LOCATION.getRequestCode());
        } else {
            // On fait en sorte que ça prenne la dernière localisation
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 100, this);
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 100, 100, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 100, this);

            // Dessin des points de repère de la map
            mapPoints.dessinerPoints(getApplicationContext(), this.googleMap);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // Position de l'ISTIC pour le virtual device : longitude[-1.638]; latitude[48.116]
        LatLng pos = new LatLng(location.getLatitude(), location.getLongitude());

        // Si on a déjà mis le curseur...
        if(currentMarker != null){
            // ... on le supprime de la carte
            currentMarker.remove();
        }

        currentMarker = this.googleMap.addMarker(
                new MarkerOptions()
                .position(pos)
                .title(currentUsername)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .draggable(true)
        );
        this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(pos.latitude, pos.longitude), 15.0f));
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        // BEGIN_INCLUDE(onRequestPermissionsResult)
        if (requestCode == PermissionRequest.ACCESS_COARSE_LOCATION.getRequestCode() || requestCode == PermissionRequest.ACCESS_FINE_LOCATION.getRequestCode()) {
            // Request for CALL_PHONE permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initialize();
            } else {
                // Permission request was denied.
                Snackbar.make(map, "Autorisation d'accès à la fonction de localisation refusée, impossible d'afficher votre position",
                        Snackbar.LENGTH_LONG)
                        .show();
            }
        }
        // END_INCLUDE(onRequestPermissionsResult)
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.map_back:
                onBackPressed();
            break;

            default:
            break;
        }
    }
}
package net.huitel.pucapab.ui.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import net.huitel.pucapab.R;
import net.huitel.pucapab.database.beans.Product;
import net.huitel.pucapab.database.dao.ProductsDAO;
import net.huitel.pucapab.recycler.RecyclerFragmentAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jérémy on 13/02/2018.
 */

public class RecyclerFragment extends Fragment {
    RecyclerView            recyclerView;
    LinearLayoutManager     linearLayoutManager;
    RecyclerFragmentAdapter recyclerFragmentAdapter;

    EditText                searchEditText;

    ProductsDAO             productsDAO;

    List<Product>           productList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_fragment_content, container, false);

        // Liste des produits
        productList = new ArrayList<Product>();
        productsDAO = new ProductsDAO(view.getContext());
        productList = productsDAO.getProducts();

        // EditText de recherche
        searchEditText = (EditText) view.findViewById(R.id.search_recyclerView);;
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                productList.clear();

                productList.addAll(productsDAO.getProductsLike(editable.toString()));

                recyclerFragmentAdapter.notifyDataSetChanged();
            }
        });

        // RecyclerView
        recyclerView            = view.findViewById(R.id.recycler_recyclerView);
        linearLayoutManager     = new LinearLayoutManager(getActivity());

        recyclerFragmentAdapter = new RecyclerFragmentAdapter(productList, R.layout.list_items);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerFragmentAdapter);

        return view;
    }
}

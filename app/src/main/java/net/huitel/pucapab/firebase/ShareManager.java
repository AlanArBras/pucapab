package net.huitel.pucapab.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 2/13/18.
 */

public class ShareManager {
    private FirebaseAuth mFirebaseAuth;
    private String mUserId;
    private DatabaseReference mSharesRef;
    private ShareManager shareManager;

    public static final String SHARES_CHILD = "shares";

    private ShareManager() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mUserId = mFirebaseAuth.getCurrentUser().getUid();
        mSharesRef = FirebaseDatabase.getInstance().getReference().child(SHARES_CHILD);
    }

    public ShareManager getInstance() {
        if (shareManager == null)
            shareManager = new ShareManager();
        return shareManager;
    }


    public void shareToUser(String userId, SharedProduct product) {
        product.toMap();
        mSharesRef.child(userId);
    }

    public SharedProduct getSharedProduct() {
        mSharesRef.child(mUserId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        Map<String, Object> postValues = new HashMap<String, Object>();
                                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                            postValues.put(snapshot.getKey(), snapshot.getValue());
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                }
                );
        return null;
    }
}

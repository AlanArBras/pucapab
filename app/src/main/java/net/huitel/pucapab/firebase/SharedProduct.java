package net.huitel.pucapab.firebase;

import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 2/13/18.
 */

public class SharedProduct {
    //TODO Make a full SharedProduct with all fields (barcode, gluten, name, image)
    private String barcode;

    private String senderId;

    private String recipientId;

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public SharedProduct(String barcode, String senderId, String recipientId){
        this.barcode = barcode;
        this.senderId = senderId;
        this.recipientId = recipientId;
    }

    public SharedProduct(String barcode, FirebaseUser sender, String recipientId){
        this.barcode = barcode;
        this.senderId = sender.getUid();
        this.recipientId = recipientId;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("barcode", barcode);
        return result;
    }
}

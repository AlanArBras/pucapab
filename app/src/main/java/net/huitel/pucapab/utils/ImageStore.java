package net.huitel.pucapab.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.huitel.pucapab.database.beans.Product;
import net.huitel.pucapab.database.dao.ProductsDAO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Djowalker on 13/02/2018.
 */

public class ImageStore {
    public static Target picassoImageTarget(Context context, final String imageDir, final String imageName, final Product product, final ProductsDAO dao) {
        ContextWrapper cw = new ContextWrapper(context);
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName); // Create image file
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            product.setImagePath(myImageFile.getAbsolutePath());
                            dao.insert(product);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());

                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {}
            }
        };
    }

    public static String imageSave(Context context, final String imageDir, final String imageName, final Product product, final ProductsDAO dao, Bitmap bitmap) {
        ContextWrapper cw = new ContextWrapper(context);
        String pathFile;
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);

            final File myImageFile = new File(directory, imageName); // Create image file
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(myImageFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                product.setImagePath(myImageFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());

            return product.getImagePath();

    }
}

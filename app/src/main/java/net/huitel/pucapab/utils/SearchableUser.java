package net.huitel.pucapab.utils;

import net.huitel.pucapab.firebase.User;

import ir.mirrajabi.searchdialog.core.Searchable;

/**
 * Created by root on 2/15/18.
 */

public class SearchableUser implements Searchable {
    private User mUser;

    public SearchableUser(User user){
        this.mUser = user;
    }

    @Override
    public String getTitle() {
        return mUser.getName().isEmpty() ? mUser.getEmail().substring(0,mUser.getEmail().indexOf('@')-1) : mUser.getName();
    }

    public SearchableUser setUser(User user) {
        mUser = user;
        return this;
    }
}

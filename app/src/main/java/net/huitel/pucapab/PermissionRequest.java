package net.huitel.pucapab;

import android.Manifest;

/**
 * Created by root on 1/25/18.
 */

public enum PermissionRequest {

    CAMERA, INTERNET, CALL_PHONE, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION,WRITE_EXTERNAL_STORAGE, READ_CONTACTS;


    private int requestCode;
    private String manifestPermission;
    private String explanation;

    static {
        CAMERA.manifestPermission = Manifest.permission.CAMERA;
        CAMERA.explanation = "L'accès à la caméra est nécessaire pour pouvoir scanner le code barre des articles à vérifier.";
        CAMERA.requestCode = 0;
        INTERNET.manifestPermission = Manifest.permission.INTERNET;
        INTERNET.explanation = "L'accès à internet est nécessaire pour obtenir les informations sur les produits scannés.";
        INTERNET.requestCode = 1;
        CALL_PHONE.manifestPermission = Manifest.permission.CALL_PHONE;
        CALL_PHONE.explanation = "L'accès à la fonction d'appel sert à appeler les pompiers en cas de problème.";
        CALL_PHONE.requestCode = 2;
        ACCESS_FINE_LOCATION.manifestPermission = Manifest.permission.ACCESS_FINE_LOCATION;
        ACCESS_FINE_LOCATION.explanation = "L'accès à la fonction de géolocalisation permet de t'aider.";
        ACCESS_FINE_LOCATION.requestCode = 3;
        ACCESS_COARSE_LOCATION.manifestPermission = Manifest.permission.ACCESS_COARSE_LOCATION;
        ACCESS_COARSE_LOCATION.explanation = "L'accès à la fonction de géolocalisation permet de t'aider.";
        ACCESS_COARSE_LOCATION.requestCode = 4;
        WRITE_EXTERNAL_STORAGE.manifestPermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        WRITE_EXTERNAL_STORAGE.explanation = "L'accès à la fonction de stockage permet de stocker les images en local pour y accéder hors connexion et plus de fluidité";
        WRITE_EXTERNAL_STORAGE.requestCode = 5;
        READ_CONTACTS.manifestPermission = Manifest.permission.READ_CONTACTS;
        READ_CONTACTS.explanation = "L'accès aux contacts permet de sélectionner un contact à appeler en cas d'urgence";
        READ_CONTACTS.requestCode = 6;
    }

    public String getManifestPermission() {
        return this.manifestPermission;
    }

    public String getExplanation() {
        return this.explanation;
    }

    public int getRequestCode() {
        return requestCode;
    }


}
